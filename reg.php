<?php
   session_start();
    //print_r($_POST);
    require_once("includes/dbconnection.php");
   
    if(isset($_POST['register'])) {
      $sn = rand(1111,9999);       
      $firstname = mysqli_real_escape_string($conn, $_POST['firstname']);
      $lastname = mysqli_real_escape_string($conn, $_POST['lastname']);
      $email = mysqli_real_escape_string($conn, $_POST['email']);
      $position = mysqli_real_escape_string($conn, $_POST['position']);
      $photo =strtolower($_FILES['photo']['name']);
      
      $file_ext = substr($photo, strpos($photo, '.'));
      $path = 'uploads/images'.$file_ext;
      $success = move_uploaded_file($_FILES['photo']['tmp_name'], $path); 
     
        if((empty($firstname)) || (empty($lastname)) || (empty($email)) || (empty($salary))){
          $_SESSION['errmssg'] = "Fields cant be empty";
        }

       if((!empty($firstname)) && (!empty($lastname)) && (!empty($email))){
        $query = "INSERT INTO registration (serial_no, firstname, lastname, email, position) 
        VALUES ('$sn', '$firstname', '$lastname', '$email', '$position')";
        $result = mysqli_query($conn, $query);
        $_SESSION['success'] = "Staff Added Successfully";
        header("location: index.php"); 
        exit;
      }  
    }      
?>     
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <title>Staff registration page</title>
    </head>
    <body>  
        <!---=====================Register Form is right here=====--->
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-3 mt-5">
                    <div class="card shadow-lg">
                        <div class="card-body">
                            <form method="POST" action="reg.php" enctype="multipart/formdata">
                                <input type="hidden" name="submit" value="anything">
                                  <h4 class="text-center"><b>ADD STAFF</b>
                                   </h4><hr>
                                   <!--Show errors--->
                                   <?php if(isset($_SESSION['errmssg'])){
                                      ?><div class="alert alert-info alert-dissimible">
                                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                                          <?php echo $_SESSION['errmssg']; ?>
                                          <?php unset($_SESSION['errmssg']); ?>
                                        </div> 
                                    <?php } ?> 
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="text-light-white">First Name</label>
                                        <input type="text" name="firstname"
                                               class="form-control"
                                               placeholder="first name">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="text-light-white">Last Name</label>
                                        <input type="text" name="lastname"
                                               class="form-control"
                                               placeholder="last name">
                                      </div>
                                    </div>
                                  </div> 
                                  <div class="form-group">
                                    <label class="text-light-white">Email Adress*</label>
                                    <input type="email" name="email"
                                           class="form-control"
                                           placeholder="email address">
                                  </div>
                                <!--   <div class="form-group">
                                    <label class="text-light-white">Salary*</label>
                                    <input type="number" name="salary_amt"
                                           class="form-control"
                                           placeholder="monthly salary">
                                  </div> -->
                                   <div class="form-group">
                                      <label class="text-light-white">Position Held*</label>
                                      <input type="text" name="position"
                                             class="form-control"
                                             placeholder="position">
                                    </div>
                                  <div class="form-group">
                                      <label class="text-light-white">Profile Pics*</label>
                                      <input type="file" name="photo"
                                             class="form-control">
  
                                    </div> 
                                  <div class="form-group text-center">
                                      <button class="btn btn-block btn-secondary" name="register" type="submit">
                                      ADD STAFF</button>
                                  </div>
                            </form>
                            <div class="text-center text-muted pt-3">
                                <a href="index.php" class="btn btn-block btn-info">ADMIN ACCESS</a>
                                <p><a href="#">Terms & conditions</a>|<a href="#">Privacy & Policy</a></p>
                            </div>
                     <!---=====================End of Register Form=====--->
                        </div>
                    </div>    
                </div>
            </div>  
        </div>
        <?php include("includes/scripts.php"); ?>

    </body>
</html>

