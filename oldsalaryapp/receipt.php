<?php
	session_start();
	include("includes/dbconnection.php");
	if($_GET){
		if(isset($_GET['id'])){
			$id = $_GET['id'];
		}
	}
		// using the id to generated the content..
		$query11 = "SELECT * FROM salary_tab WHERE id='$id'";
		$result11 = mysqli_query($conn, $query11);
		$row = mysqli_fetch_array($result11);
		$staffid = $row['sn'];
		$firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $email = $row['email'];
        $salary_payment = $row['salary'];
        $requested = $row['advance'];
        $expected_payment = $row['expected'];
        $position  = $row['position'];
        $bonus = $row['bonus'];
        $deduct = $row['deduct'];
 ?>
 <?php include("includes/navheader.php"); ?>
 <div class="container">
            <div class="row">
                <div class="col-md-6 offset-3 mt-5 pt-5">
                    <div class="card shadow-lg">
                    	<div class="card-header">Monthly Payment Slip</div>
                        <div class="card-body">
                            <h5>Staffid: <?php echo $staffid ?><br>
                            	First Name: <?php echo $firstname ?><br>
                            	Last Name: <?php echo $lastname ?><br>
                            	Email: <?php echo $email ?><br>
                            	Position: <?php echo $position ?><br>
                            </h5>
                            <div class="card-header mt-3">Salary details</div>
                            <h5> Total Salary: N<?php echo $salary_payment ?><br>
                            	 Total Bonus: N<?php echo $bonus ?><br>
                            	 Total Deduct: N<?php echo $deduct ?><br>
                            	 Total Expected Payment: N<?php echo $requested ?><br>
                            </h5>
                            <input type="button" onclick="window.print();" class="btn btn-block btn-success mt-3" value="Print"/><br><br>
                        </div>
                    </div>    
                </div>
            </div>  
        </div>
        
        <?php include("includes/scripts.php"); ?>