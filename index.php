<?php
    session_start();
    require_once("includes/dbconnection.php");  
 ?>
<?php include("includes/navheader.php"); ?>
 <!----Pages Designs will be inserted here -->
   <div class="container pt-4 mt-5">
        <div class="row">  
            <div class="col-sm-12">
               <?php if(isset($_SESSION['message'])){
                  ?><div class="alert alert-info alert-dissimible">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <?php echo $_SESSION['message']; ?>
                      <?php unset($_SESSION['message']); ?>
                    </div> 
                <?php } ?>
                <div class="alert alert-info">
                    <h4 class=""><b>Admin PageAccess</b></h4>
                </div>
                <div class="table-responsive-sm">
                  <table class="table table-striped">
                    <p class="ml-2"><b>All Staffs Member Table</b></p>
                    <thead class="thead-dark">
                      <tr>
                        <th>ID</th>
                        <th>STAFFID</th>
                        <th>FullName</th>
                        <th>Email</th>
                        <th>Salary</th>
                        <th>Advance Payment</th>
                        <th>Bonus</th>
                        <th>Deduct</th>
                        <th>Expected Payment</th>
                        <th>Action</th>
                        <th>Pay</th>
                        <th>Status</th>
                        <th>Print</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php 
                            $mm = date('m');
                            $query = "SELECT 
                             `registration`.`reg_id`,
                             `registration`.`serial_no`,
                             `registration`.`firstname`,
                             `registration`.`lastname`,
                             `registration`.`email`,
                              `registration`.`firstname`,
                              `transact`.`salary`,
                              `transact`.`advance`,
                              `transact`.`bonus`,
                              `transact`.`deduct`,
                               `transact`.`expected`,
                               `transact`.`reg_id` as `transact_reg_id`
                              FROM `registration` JOIN `transact` ON  `registration`.`reg_id`=`transact`.`reg_id`"; 
                            $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
                            while($row = mysqli_fetch_array($result)){
                                ?>
                              <tr class="text-dark">
                                  <td><?php echo $row['reg_id']; ?></td>
                                  <td><?php echo $row['serial_no']; ?></td>
                                  <td><?php echo $row['firstname'] ." ". $row['lastname'] ?></td>
                                  <td><?php echo $row['email']; ?></td>
                                  <td><?php echo $row['salary']; ?></td>
                                  <td><?php echo $row['advance']; ?></td>
                                  <td><?php echo $row['bonus']; ?></td>
                                  <td><?php echo $row['deduct']; ?></td>
                                  <td><?php echo $row['expected']; ?></td>
                                  <td><?php echo "<a class='btn btn-sm btn-success' 
                                  href='profile.php?reg_id=".$row['reg_id']."'>Profile</a>" ?>
                                  <td><a class='btn btn-sm btn-danger'>Pay</a></td>
                                 <td><button class="btn btn-sm btn-info"></button></td> 
                                 <td><a class='btn btn-sm btn-secondary'>Print</a></td> 
                               </tr>  
                            <?php } ?>
                    </tbody>
                  </table>
                  <div class="">
                    <p>Total Salary is : </b>
                       Total Advance Salary is : </b>
                       Total Expected Salary  is : </b>
                       Total Bonus  is :  </b>
                       Total Deduct  is : </p>
                    <a href="reg.php" class="btn btn block btn-info">Add Staff</a> 
                  </div>
                      
          </div> 
        </div>
   </div>
 </body>
</html>
<?php include("includes/scripts.php")?>

  <!--<tr><td colspan="10" class="text-center text-white bg-secondary">No Course Added Yet!!</td></tr>-->