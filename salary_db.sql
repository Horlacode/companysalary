-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2020 at 08:59 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salary_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `reg_id` int(11) NOT NULL,
  `serial_no` varchar(5) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `profile_pics` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`reg_id`, `serial_no`, `firstname`, `lastname`, `email`, `position`, `username`, `password`, `profile_pics`) VALUES
(1, '3149', 'Tolulope', 'Ajao', 'olasunkanmiadu33@gmail.com', 'IT officer', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `salary_tab`
--

CREATE TABLE `salary_tab` (
  `id` int(11) NOT NULL,
  `sn` varchar(5) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `salary` int(11) NOT NULL,
  `advance` int(11) NOT NULL,
  `expected` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `bonus` int(11) NOT NULL,
  `deduct` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_pics` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `dd` int(10) NOT NULL,
  `mm` int(10) NOT NULL,
  `yy` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary_tab`
--

INSERT INTO `salary_tab` (`id`, `sn`, `firstname`, `lastname`, `email`, `salary`, `advance`, `expected`, `position`, `status`, `bonus`, `deduct`, `username`, `password`, `profile_pics`, `created`, `dd`, `mm`, `yy`) VALUES
(1, '6578', 'Trenet', 'Company', 'trenet@gmail.com', 10000, 14000, 4000, 'Admin', 'Paid', 168000, 160000, 'adminuser', 'testing123', '', '2020-06-15 16:10:49', 18, 7, 20),
(2, '3540', 'Adu', 'Sunkanmi', 'olasunkanmiadu33@gmail.com', 5000, 6000, 0, '', 'Paid', 1000, 0, '', '', '', '2020-06-15 16:47:40', 16, 6, 20),
(3, '8973', 'Adewole', 'Jude', 'jude@gmail.com', 5000, 2000, 3000, '', 'Paid', 0, 0, '', '', '', '2020-06-15 17:35:34', 16, 6, 20),
(9, '1625', 'Abidakun', 'Seun', 'seun@gmail.com', 9000, 2000, 8000, '', 'Paid', 1000, 0, '', '', '', '2020-06-15 19:57:10', 16, 6, 20),
(11, '9324', 'Ajebode', 'learn', 'learn@gmail.com', 2000, 5500, 12500, '', '', 16000, 0, '', '', '', '2020-06-15 20:06:47', 16, 6, 20),
(12, '7433', 'Estther', 'Beauty', 'esther@gmail.com', 5000, 3000, 21000, '', '', 24000, 5000, '', '', '', '2020-06-15 20:09:35', 16, 6, 20);

-- --------------------------------------------------------

--
-- Table structure for table `salary_tb`
--

CREATE TABLE `salary_tb` (
  `id` int(11) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `salary` float NOT NULL,
  `advance` varchar(11) NOT NULL,
  `expected` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary_tb`
--

INSERT INTO `salary_tb` (`id`, `firstname`, `lastname`, `email`, `salary`, `advance`, `expected`, `username`, `password`, `created`) VALUES
(1, 'Trenet', 'Company', 'Trenet@gmail.com', 0, '', 0, 'adminuser', 'testing123', '2020-06-15 15:52:51'),
(17, 'Trenet', 'Company', 'trenet@gmail.com', 0, '', 0, 'adminuser', 'testing123', '2020-06-15 15:54:17'),
(18, 'Trenet', 'Company', 'trenet@gmail.com', 0, '', 0, 'adminuser', 'testing123', '2020-06-15 15:55:22');

-- --------------------------------------------------------

--
-- Table structure for table `transact`
--

CREATE TABLE `transact` (
  `trans_id` int(11) NOT NULL,
  `reg_id` int(11) DEFAULT NULL,
  `salary` int(11) NOT NULL,
  `advance` int(11) NOT NULL,
  `expected` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `deduct` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `dd` int(2) NOT NULL,
  `mm` int(2) NOT NULL,
  `yy` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transact`
--

INSERT INTO `transact` (`trans_id`, `reg_id`, `salary`, `advance`, `expected`, `bonus`, `deduct`, `status`, `dd`, `mm`, `yy`) VALUES
(1, 1, 200000, 2000, 0, 20000, 20000, '', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `salary_tab`
--
ALTER TABLE `salary_tab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_tb`
--
ALTER TABLE `salary_tb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transact`
--
ALTER TABLE `transact`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `reg_id` (`reg_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `salary_tab`
--
ALTER TABLE `salary_tab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `salary_tb`
--
ALTER TABLE `salary_tb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `transact`
--
ALTER TABLE `transact`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transact`
--
ALTER TABLE `transact`
  ADD CONSTRAINT `transact_ibfk_1` FOREIGN KEY (`reg_id`) REFERENCES `registration` (`reg_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
